<?php

namespace Drupal\encrypt_vault_transit\Plugin\EncryptionMethod;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\encrypt\EncryptionMethodInterface;
use Drupal\encrypt\Exception\EncryptException;
use Drupal\encrypt\Plugin\EncryptionMethod\EncryptionMethodBase;
use Drupal\vault\VaultClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class AwsKmsEncryptionMethod.
 *
 * @EncryptionMethod(
 *   id = "vault_transit",
 *   title = @Translation("Vault Transit"),
 *   description = "Encryption using Vault Transit secret backend",
 *   key_type = {"vault_transit"}
 * )
 */
class VaultTransitEncryptionMethod extends EncryptionMethodBase implements EncryptionMethodInterface, ContainerFactoryPluginInterface {

  /**
   * The settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $settings;

  /**
   * The Vault client.
   *
   * @var \Drupal\vault\VaultClientInterface
   */
  protected VaultClientInterface $vaultClient;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Constructs a VaultAwsKeyProvider object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\vault\VaultClientInterface $vault_client
   *   A Drupal Vault module client.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String Translation Service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, VaultClientInterface $vault_client, LoggerInterface $logger, TranslationInterface $string_translation) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setVaultClient($vault_client);
    $this->setLogger($logger);
    $this->setStringTranslation($string_translation);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('vault.vault_client_no_lease_storage'),
      $container->get('logger.channel.vault'),
      $container->get('string_translation')
    );
  }

  /**
   * Sets VaultClient property.
   *
   * @param \Drupal\vault\VaultClientInterface $vaultClient
   *   The KMS client.
   *
   * @return $this
   *   Current object.
   */
  public function setVaultClient(VaultClientInterface $vaultClient): static {
    $this->vaultClient = $vaultClient;
    return $this;
  }

  /**
   * Sets logger property.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   *
   * @return $this
   *   Current object.
   */
  public function setLogger(LoggerInterface $logger): static {
    $this->logger = $logger;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function checkDependencies($text = NULL, $key = NULL): array {
    $errors = [];

    if (!class_exists('\Drupal\vault\VaultClient')) {
      $errors[] = $error = $this->t('HashiCorp Vault library is not correctly installed.');
      $this->logger->error($error);
    }

    return $errors;
  }

  /**
   * {@inheritdoc}
   */
  public function encrypt($text, $key) {
    try {
      $data = [
        'name' => $key,
        'plaintext' => base64_encode($text),
      ];
      $response = $this->vaultClient->write(sprintf("/transit/encrypt/%s", $key), $data);
      $response_data = $response->getData();
      if (isset($response_data['ciphertext'])) {
        return $response_data['ciphertext'];
      }
      throw new EncryptException('no response when encrypting data');
    }
    catch (\Exception $e) {
      $this->logException($e, $text);
      throw new EncryptException('unable to encrypt data');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function decrypt($text, $key): string {
    try {
      $data = [
        'name' => $key,
        'ciphertext' => $text,
      ];
      $response = $this->vaultClient->write(sprintf("/transit/decrypt/%s", $key), $data);
      $data = $response->getData();
      if (!empty($data['plaintext'])) {
        return base64_decode($data['plaintext']);
      }
      throw new EncryptException('unexpected response when decrypting ' . $key);
    }
    catch (\Exception $e) {
      $this->logException($e, $text);
      throw new EncryptException('unexpected response when decrypting ' . $key);
    }
  }

  /**
   * Helper method for logging exceptions.
   *
   * @param \Exception $e
   *   The exception.
   * @param string $plaintext
   *   Plaintext data to redact from logs.
   */
  public function logException(\Exception $e, string $plaintext): void {
    $placeholder = '**REDACTED**';
    $message = str_replace($plaintext, $placeholder, $e->getMessage());
    $context['sensitive_data'] = $plaintext;
    $this->logger->error($message, $context);
  }

}
