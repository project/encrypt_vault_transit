<?php

namespace Drupal\Tests\encrypt_vault_transit\Unit;

use Drupal\encrypt_vault_transit\Plugin\EncryptionMethod\VaultTransitEncryptionMethod;
use Drupal\Tests\UnitTestCase;
use Drupal\vault\VaultClientInterface;
use Psr\Log\LoggerInterface;
use Vault\ResponseModels\Response;

/**
 * Tests the Transit Encryption functions.
 *
 * @group encrypt_vault_transit
 *
 * @covers \Drupal\encrypt_vault_transit\Plugin\EncryptionMethod\VaultTransitEncryptionMethod
 */
class TransitEncryptionMethodTest extends UnitTestCase {

  /**
   * Vault Client mock.
   *
   * @var \Drupal\vault\VaultClientInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $vaultClientMock;

  /**
   * Vault Transit Encryption Method plugin.
   *
   * @var \Drupal\encrypt_vault_transit\Plugin\EncryptionMethod\VaultTransitEncryptionMethod
   */
  protected VaultTransitEncryptionMethod $plugin;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->vaultClientMock = $this->createMock(VaultClientInterface::class);

    $logger_mock = $this->createMock(LoggerInterface::class);

    $this->plugin = new VaultTransitEncryptionMethod([], 'vault_transit', [], $this->vaultClientMock, $logger_mock, $this->getStringTranslationStub());
  }

  /**
   * Test checkDependencies()
   */
  public function testcheckDependencies(): void {
    $this->assertEmpty($this->plugin->checkDependencies(), "No dependency error");
  }

  /**
   * Test encrypt()
   */
  public function testEncrypt(): void {

    $expected_data = [
      'name' => 'test_key',
      'plaintext' => 'dGVzdF9kYXRh',
    ];

    $this->vaultClientMock
      ->method('write')
      ->with('/transit/encrypt/test_key', $expected_data)
      ->willReturnCallback([$this, 'callbackTestEncrypt']);

    $this->assertEquals('encrypted_data', $this->plugin->encrypt('test_data', 'test_key'), 'Successfully encrypt data');
    $this->expectExceptionMessage('unable to encrypt data');
    $this->plugin->encrypt('test_data', 'test_key');
  }

  /**
   * Callback for encrypt() test Vault Client mock.
   *
   * @return \Vault\ResponseModels\Response|void
   *   Response data.
   */
  public function callbackTestEncrypt() {
    static $counter = 0;
    $counter++;

    switch ($counter) {
      case 1:
        return new Response(['data' => ['ciphertext' => 'encrypted_data']]);

      case 2:
        return new Response(['data' => ['not_valid' => 'invalid_data']]);
    }
  }

  /**
   * Test decrypt()
   */
  public function testDecrypt(): void {
    $expeted_data = [
      'name' => 'test_key',
      'ciphertext' => 'test_encrypted_data',
    ];
    $this->vaultClientMock
      ->method('write')
      ->with('/transit/decrypt/test_key', $expeted_data)
      ->willReturnCallback([$this, 'callbackTestDecrypt']);

    $this->assertEquals('test_data', $this->plugin->decrypt('test_encrypted_data', 'test_key'), 'Successfully decrypted data');
    $this->expectExceptionMessage('unexpected response when decrypting test_key');
    $this->plugin->decrypt('test_encrypted_data', 'test_key');
  }

  /**
   * Callback for decrypt() test Vault Client mock.
   *
   * @return \Vault\ResponseModels\Response|void
   *   Response data.
   */
  public function callbackTestDecrypt() {
    static $counter = 0;
    $counter++;

    switch ($counter) {
      case 1:
        return new Response(['data' => ['plaintext' => 'dGVzdF9kYXRh']]);

      case 2:
        return new Response(['data' => ['not_valid' => 'invalid_data']]);
    }
  }

}
