CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers
* Previous Maintainers

INTRODUCTION
------------

This module provides support for the Encrypt module to utilize the Vault Transit
secrets engine to provide encrypt/decrypt services.

Vault will securely maintain the encryption/decryption keys outside of Drupal.

An example Vault encrypted is key is for the string 'test' the value of 'vault:
v1:aNy6NRuUNi8+hDZ305WPmAImDrFAGHz23RThoBQ9kOE=' indicating that this is a Vault
transit key, and it is using Version 1 of the key in Vault.

For more details on the Transit engine in HashiCorp Vault
see https://learn.hashicorp.com/tutorials/vault/eaas-transit

* For the full description of the module visit:
  https://www.drupal.org/project/encrypt_vault_transit

* To submit bug reports and feature suggestions, or to track changes visit:
  https://www.drupal.org/project/issues/encrypt_vault_transit

REQUIREMENTS
------------

This module requires the following modules:

* [Vault](https://www.drupal.org/project/issues/vault)
* [Key](https://www.drupal.org/project/key)
* [Encrypt](https://www.drupal.org/project/encrypt)

INSTALLATION
------------

Install the Vault Transit Encryption module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

CONFIGURATION
-------------

1. If you do not already have a Transit key configured in Vault do so now.

   The encryption key must NOT use the derived or convergent options in Vault.  
   Please refer to the Vault manual for more details on creating a Transit key.

2. Add a Vault Transit Key at admin/config/system/keys/add.

   The value of the key should be the name of the Transit Key that exists in
   Vault.  
   Please refer to the Key Module documentation for more details on configuring
   keys.  

3. Create a new Encryption Profile at
   admin/config/system/encryption/profiles/add

   For the Encryption method select 'Vault Transit'.  
   For the Encryption Key select the key from Step 1.

4. Validate the profile works by using the Encryption Module profile test
   feature.

MAINTAINERS
-----------

* Conrad Lara - https://www.drupal.org/u/cmlara

PREVIOUS MAINTAINERS
--------------------
Nick Santamaria - https://www.drupal.org/u/nicksanta
